#!/bin/bash
CONFIG_FILE=/root/.transcendence/transcendence.conf
NODE_IP=$(curl -4 icanhazip.com)
# Update config file
if [ -z $MASTERNODE_GENKEY ]; then
    echo "MASTERNODE_GENKEY environment variable has to bet set!"
    exit 1
fi
sed -i 's/MN_GENKEY/'"$MASTERNODE_GENKEY"'/g' $CONFIG_FILE
sed -i 's/NODE_IP/'"$NODE_IP"'/g' $CONFIG_FILE
# Start daemon
exec /root/transcendenced