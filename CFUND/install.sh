#!/bin/bash

#
# crowdfuncoin install script v1.0.0.1
# (c) 2018 twistedmonkey
# nuno@twistedmonkey.org
#


TMP_FOLDER=$(mktemp -d)
CONFIG_FILE='CrowdFundingCoin.conf'
CONFIGFOLDER='/root/.CrowdFundingCoin'
COIN_DAEMON='CrowdFundingCoind'
COIN_CLI='CrowdFundingCoin-cli'
COIN_PATH='/usr/local/bin/'
COIN_TGZ='latest-linux.tar.gz'
COIN_NAME='CrowdFundingCoin'
COIN_URL='https://download.crowdfundcoin.io/latest-linux.tar.gz'
COIN_PORT=47755
RPC_PORT=47756
NODEIP=$(curl -s4 icanhazip.com)

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

function download_node() {
  echo -e "Prepare to download $COIN_NAME binaries"
  cd $TMP_FOLDER
  wget $COIN_URL
  tar xvzf $COIN_TGZ
  chmod a+x bin/*
  mv bin/* /usr/local/bin/
  cd - >/dev/null 2>&1
  rm -r $TMP_FOLDER >/dev/null 2>&1
  clear
}


function create_config() {
  mkdir $CONFIGFOLDER >/dev/null 2>&1
  RPCUSER=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w10 | head -n1)
  RPCPASSWORD=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w22 | head -n1)
  cat << EOF > $CONFIGFOLDER/$CONFIG_FILE
rpcuser=$RPCUSER
rpcpassword=$RPCPASSWORD
rpcallowip=127.0.0.1
listen=1
server=1
daemon=0
port=$COIN_PORT
logintimestamps=1
maxconnections=256
#bind=$NODEIP
masternode=1
externalip=NODE_IP:$COIN_PORT
masternodeprivkey=MN_GENKEY
EOF
for i in `host dnsseed.crowdfundcoin.io| cut -d\  -f4`; do echo addnode=$i >> $CONFIGFOLDER/$CONFIG_FILE; done
}


function checks() {
version=$(lsb_release -sr)

#check for version of ubuntu
case $version in
16.04)
    echo "Ubuntu 16.04 detected..."
    ;;
18.04)
    echo "Ubuntu 18.04 detected..."
    ;;
*)
    echo "${RED}This version $version is unsupported..."
    echo "You should install this software on Ubuntu 16.04 or 18.04${NC}"
    exit 1;
esac

if [[ $EUID -ne 0 ]]; then
   echo -e "${RED}$0 must be run as root.${NC}"
   exit 1
fi

if [ -n "$(pidof $COIN_DAEMON)" ] || [ -e "$COIN_DAEMOM" ] ; then
  echo -e "${RED}$COIN_NAME is already installed.${NC}"
  echo -e "in order to reinstall you should stop the daemon first."
  exit 1
fi
}

function prepare_system() {
echo -e "Prepare the system to install ${GREEN}$COIN_NAME${NC} master node."
apt -y update >/dev/null 2>&1
DEBIAN_FRONTEND=noninteractive apti -y update > /dev/null 2>&1
DEBIAN_FRONTEND=noninteractive apt -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y -qq upgrade >/dev/null 2>&1
apt install -y software-properties-common >/dev/null 2>&1
echo -e "${GREEN}Adding bitcoin PPA repository"
apt-add-repository -y ppa:bitcoin/bitcoin >/dev/null 2>&1
echo -e "Installing required packages, it may take some time to finish.${NC}"
apt -y update >/dev/null 2>&1
apt -y install -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" make software-properties-common \
build-essential libtool autoconf libssl-dev libboost-dev libboost-chrono-dev libboost-filesystem-dev libboost-program-options-dev \
libboost-system-dev libboost-test-dev libboost-thread-dev sudo automake git wget curl libdb4.8-dev bsdmainutils libdb4.8++-dev \
libminiupnpc-dev libgmp3-dev ufw pkg-config libevent-dev  libdb5.3++ libzmq5 >/dev/null 2>&1
if [ "$?" -gt "0" ];
  then
    echo -e "${RED}Not all required packages were installed properly. Try to install them manually by running the following commands:${NC}\n"
    echo "apt -y update"
    echo "apt -y install software-properties-common"
    echo "apt-add-repository -y ppa:bitcoin/bitcoin"
    echo "apt -y update"
    echo "apt install -y make build-essential libtool software-properties-common autoconf libssl-dev libboost-dev libboost-chrono-dev libboost-filesystem-dev \
libboost-program-options-dev libboost-system-dev libboost-test-dev libboost-thread-dev sudo automake git curl libdb4.8-dev \
bsdmainutils libdb4.8++-dev libminiupnpc-dev libgmp3-dev ufw fail2ban pkg-config libevent-dev libzmq5"
 exit 1
fi

clear
}


##### Main #####
clear

checks
prepare_system
download_node
create_config