#!/bin/bash
# Start cron as root
cron
# Update config file
CONFIG_FILE=/home/psc/.primestone/primestone.conf
NODE_IP=$(curl -4 icanhazip.com)
if [ -z $MASTERNODE_GENKEY ]; then
    echo "MASTERNODE_GENKEY environment variable has to bet set!"
    exit 1
fi
sed -i 's/MN_GENKEY/'"$MASTERNODE_GENKEY"'/g' $CONFIG_FILE
sed -i 's/NODE_IP/'"$NODE_IP"'/g' $CONFIG_FILE
export NODE_PORT=${NODE_PORT:-34124}
sed -i 's/NODE_PORT/'"$NODE_PORT"'/g' $CONFIG_FILE
# Start daemon
exec su - psc -c /home/psc/primestoned