#/bin/bash
NONE='\033[00m'
RED='\033[01;31m'
GREEN='\033[01;32m'
YELLOW='\033[01;33m'
BOLD='\033[1m'
STEPS=10

COINDOWNLOADLINK1=https://github.com/exdexdev/exdex/releases/download/v1.0.0/exdexd
COINDOWNLOADLINK2=https://github.com/exdexdev/exdex/releases/download/v1.0.0/exdex-cli
COINDOWNLOADLINK3=https://github.com/exdexdev/exdex/releases/download/v1.0.0/exdex-tx
COINPORT=19002
COINRPCPORT=19012
COINDAEMON=exdexd
COINDAEMONCLI=exdex-cli
COINDAEMONTX=exdex-tx
COINCORE=.exdex
COINCONFIG=exdex.conf

updateAndUpgrade() {
    echo
    echo "[2/${STEPS}] Running update and upgrade. This may take a minute..."
    sudo DEBIAN_FRONTEND=noninteractive apt-get update -qq -y > /dev/null 2>&1
    sudo DEBIAN_FRONTEND=noninteractive apt-get upgrade -y -qq > /dev/null 2>&1
    echo -e "${GREEN}* Done${NONE}";
}

installDependencies() {
    echo
    echo -e "[3/${STEPS}] Installing dependecies. This will take a few minutes..."
    sudo apt-get install bc git nano rpl wget python-virtualenv -qq -y > /dev/null 2>&1
    sudo apt-get install build-essential libtool automake autoconf -qq -y > /dev/null 2>&1
    sudo apt-get install autotools-dev autoconf pkg-config libssl-dev -qq -y > /dev/null 2>&1
    sudo apt-get install libgmp3-dev libevent-dev bsdmainutils libboost-all-dev -qq -y > /dev/null 2>&1
    sudo apt-get install software-properties-common python-software-properties -qq -y > /dev/null 2>&1
    sudo add-apt-repository ppa:bitcoin/bitcoin -y > /dev/null 2>&1
    sudo apt-get update -qq -y > /dev/null 2>&1
    sudo apt-get upgrade -qq -y > /dev/null 2>&1
    sudo apt-get install libdb4.8-dev libdb4.8++-dev -qq -y > /dev/null 2>&1
    sudo apt-get install libminiupnpc-dev -qq -y > /dev/null 2>&1
    sudo apt-get install libzmq5 -qq -y > /dev/null 2>&1
    sudo apt-get install virtualenv -qq -y > /dev/null 2>&1
    sudo apt-get update -qq -y > /dev/null 2>&1
    sudo apt-get upgrade -qq -y > /dev/null 2>&1
    echo -e "${NONE}${GREEN}* Done${NONE}";
}

downloadWallet() {
    echo
    echo -e "[7/${STEPS}] Downloading wallet..."

    wget $COINDOWNLOADLINK1 $COINDOWNLOADLINK2 $COINDOWNLOADLINK3 > /dev/null 2>&1
    chmod 755 $COINDAEMON $COINDAEMONCLI $COINDAEMONTX > /dev/null 2>&1

    echo -e "${NONE}${GREEN}* Done${NONE}";
}

installWallet() {
    echo
    echo -e "[8/${STEPS}] Installing wallet..."
    strip $COINDAEMON $COINDAEMONCLI $COINDAEMONTX > /dev/null 2>&1
    sudo mv -t /usr/bin $COINDAEMON $COINDAEMONCLI $COINDAEMONTX > /dev/null 2>&1
    cd
    echo -e "${NONE}${GREEN}* Done${NONE}";
}

configureWallet() {
    echo
    echo -e "[9/${STEPS}] Configuring wallet..."
#    $COINDAEMON -daemon > /dev/null 2>&1
#    sleep 10
#    $COINDAEMONCLI stop > /dev/null 2>&1
#    sleep 5

#    mnip=$(curl --silent ipinfo.io/ip)
    rpcuser=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`
    rpcpass=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`

    mkdir ~/$COINCORE

#    $COINDAEMON > /dev/null 2>&1
#    sleep 10

#    read -e -p "Enter your Masternode Private Key: " mnkey
    port=$(echo "$COINPORT")
    rpcport=$(echo "$COINRPCPORT")

#    $COINDAEMONCLI stop > /dev/null 2>&1
#    sleep 5

    echo -e "rpcuser=${rpcuser}\nrpcpassword=${rpcpass}\nrpcallowip=127.0.0.1\nport=${port}\nrpcport=${rpcport}\ndaemon=0\nserver=1\nlisten=1\nlogtimestamps=1\nmaxconnections=256\nmasternode=1\nexternalip=NODE_IP\nmasternodeprivkey=MN_GENKEY" > ~/$COINCORE/$COINCONFIG

    echo -e "${NONE}${GREEN}* Done${NONE}";
}

clear
cd

echo
echo -e "------------------------------------------------------------"
echo -e "|                                                          |"
echo -e "|        ${BOLD}---- EXdex masternode installer ----${NONE}        |"
echo -e "|                                                          |"
echo -e "------------------------------------------------------------"

echo -e "${BOLD}"
echo -e "This script will install a EXdex masternode wallet on your VPS."
echo -e "${NONE}"

updateAndUpgrade
installDependencies
downloadWallet
installWallet
configureWallet

echo && echo -e "${BOLD}The VPS wallet for your masternode has been successfully created. The following data needs to be recorded in your local masternode configuration file:${NONE}"
echo && echo -e "${BOLD}${YELLOW} 1) Masternode_IP: ${mnip}:${COINPORT}${NONE}"
echo && echo -e "${BOLD}${YELLOW} 2) Masternode_Private_Key: ${mnkey}${NONE}"
echo && echo -e "${BOLD}${GREEN}Copy the above two yellow outputs into the text document you created earlier. Then logout and close this terminal console and continue with the rest of the masternode setup guide${NONE}" && echo