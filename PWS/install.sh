#!/bin/bash

TMP_FOLDER=$(mktemp -d)
CONFIG_FILE="PowerSwitch.conf"
CONFIG_FOLDER=".PowerSwitch"
BINARY_FILE="/usr/local/bin/PowerSwitchd"
PWS_REPO="https://github.com/PowerSwitchCrypto/Wallet.git"
COIN_TGZ='https://www.powerswitch.me/binaries/PowerSwitchd.gz'

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'


function compile_error() {
if [ "$?" -gt "0" ];
 then
  echo -e "${RED}Failed to compile $@. Please investigate.${NC}"
  exit 1
fi
}


function checks() {
if [[ $(lsb_release -d) != *16.04* ]]; then
  echo -e "${RED}You are not running Ubuntu 16.04. Installation is cancelled.${NC}"
  exit 1
fi

if [[ $EUID -ne 0 ]]; then
   echo -e "${RED}$0 must be run as root.${NC}"
   exit 1
fi
}

function deploy_binaries() {
  cd $TMP
  wget -q $COIN_TGZ >/dev/null 2>&1
  gunzip PowerSwitchd.gz >/dev/null 2>&1
  chmod +x PowerSwitchd >/dev/null 2>&1
  cp PowerSwitchd /usr/local/bin/ >/dev/null 2>&1
}

function prepare_system() {

echo -e "Prepare the system to install PowerSwitch masternode."
apt-get update >/dev/null 2>&1
DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y -qq upgrade >/dev/null 2>&1
apt install -y software-properties-common >/dev/null 2>&1
echo -e "${GREEN}Adding bitcoin PPA repository"
apt-add-repository -y ppa:bitcoin/bitcoin >/dev/null 2>&1
echo -e "Installing required packages, it may take some time to finish.${NC}"
apt-get update >/dev/null 2>&1
apt-get install -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" make software-properties-common \
build-essential libtool autoconf libssl-dev libboost-dev libboost-chrono-dev libboost-filesystem-dev libboost-program-options-dev \
libboost-system-dev libboost-test-dev libboost-thread-dev sudo automake git wget pwgen curl libdb4.8-dev bsdmainutils \
libdb4.8++-dev libminiupnpc-dev libgmp3-dev ufw pwgen
clear

clear
}

function ask_port() {
  POWERSWITCHPORT=9333
}

function ask_user() {
  POWERSWITCHUSER="powerswitch"
  useradd -m $POWERSWITCHUSER
  USERPASS=$(pwgen -s 12 1)
  echo "$POWERSWITCHUSER:$USERPASS" | chpasswd

  POWERSWITCHHOME=$(sudo -H -u $POWERSWITCHUSER bash -c 'echo $HOME')
  POWERSWITCHFOLDER="$POWERSWITCHHOME/.PowerSwitch"
  mkdir -p $POWERSWITCHFOLDER
  chown -R $POWERSWITCHUSER: $POWERSWITCHFOLDER >/dev/null
}

function create_config() {
  RPCUSER=$(pwgen -s 8 1)
  RPCPASSWORD=$(pwgen -s 15 1)
  cat << EOF > $POWERSWITCHFOLDER/$CONFIG_FILE
rpcuser=$RPCUSER
rpcpassword=$RPCPASSWORD
rpcallowip=127.0.0.1
rpcport=$[POWERSWITCHPORT-100]
listen=1
server=1
daemon=0
port=$POWERSWITCHPORT
logtimestamps=1
maxconnections=256
masternode=1
masternodeaddr=NODE_IP:$POWERSWITCHPORT
externalip=NODE_IP
masternodeprivkey=MN_GENKEY
addnode=139.99.98.127
addnode=139.99.98.128
addnode=139.99.98.129
EOF
}

function important_information() {
 echo
 echo -e "================================================================================================================================"
 echo -e "PowerSwitch Masternode is up and running as user ${GREEN}$POWERSWITCHUSER${NC} and it is listening on port ${GREEN}$POWERSWITCHPORT${NC}."
 echo -e "${GREEN}$POWERSWITCHUSER${NC} password is ${RED}$USERPASS${NC}"
 echo -e "Configuration file is: ${RED}$POWERSWITCHFOLDER/$CONFIG_FILE${NC}"
 echo -e "Start: ${RED}systemctl start $POWERSWITCHUSER.service${NC}"
 echo -e "Stop: ${RED}systemctl stop $POWERSWITCHUSER.service${NC}"
 echo -e "VPS_IP:PORT ${RED}$NODEIP:$POWERSWITCHPORT${NC}"
 echo -e "MASTERNODE PRIVATEKEY is: ${RED}$POWERSWITCHKEY${NC}"
 echo -e "================================================================================================================================"
}

function setup_node() {
  ask_user
  ask_port
  create_config
  important_information
}


##### Main #####
clear

checks
prepare_system
deploy_binaries
setup_node
