#!/bin/bash
# Update config file
CONFIG_FILE=/home/xlib/.liberty/liberty.conf
NODE_IP=$(curl -4 icanhazip.com)
if [ -z $MASTERNODE_GENKEY ]; then
    echo "MASTERNODE_GENKEY environment variable has to bet set!"
    exit 1
fi
sed -i 's/MN_GENKEY/'"$MASTERNODE_GENKEY"'/g' $CONFIG_FILE
sed -i 's/NODE_IP/'"$NODE_IP"'/g' $CONFIG_FILE
# Start daemon
exec su - xlib -c /home/xlib/libertyd