#!/bin/bash
CONFIG_FILE=/root/.elli/elli.conf
NODE_IP=$(curl -4 icanhazip.com)

# Update config file
if [ -z $MASTERNODE_GENKEY ]; then
    echo "MASTERNODE_GENKEY environment variable has to bet set!"
    exit 1
fi
sed -i 's/MN_GENKEY/'"$MASTERNODE_GENKEY"'/g' $CONFIG_FILE
sed -i 's/NODE_IP/'"$NODE_IP"'/g' $CONFIG_FILE

# Configure dupmn script
curl -sL https://raw.githubusercontent.com/neo3587/dupmn/master/dupmn_install.sh | sudo -E bash -
dupmn profadd elli.dmn Elli

IFS=',' read -r -a KEYS <<< "$MASTERNODE_KEYS"
for i in ${!KEYS[@]}
do
    dupmn install Elli -p "${KEYS[$i]}"
    ellid-$(($i+1)) &
done

# Start daemon
exec /root/ellid