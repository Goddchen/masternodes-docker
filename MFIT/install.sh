#!/bin/bash

TMP_FOLDER=$(mktemp -d)
CONFIG_FILE='mfit.conf'
CONFIGFOLDER='/root/.mfit'
COIN_DAEMON='mfitd'
COIN_CLI='mfit-cli'
COIN_PATH='/usr/local/bin/'
COIN_REPO='https://github.com/muayfitcoin/muayfitcoin.git'
COIN_TGZ='https://github.com/muayfitcoin/muayfitcoin/releases/download/v1.0.0/Linux.zip'
COIN_ZIP=$(echo $COIN_TGZ | awk -F'/' '{print $NF}')

COIN_NAME='Mfit'
COIN_PORT=1188
RPC_PORT=1189
NODEIP=$(curl -s4 icanhazip.com)

BLUE="\033[0;34m"
YELLOW="\033[0;33m"
CYAN="\033[0;36m" 
PURPLE="\033[0;35m"
RED='\033[0;31m'
GREEN="\033[0;32m"
NC='\033[0m'
MAG='\e[1;35m'

purgeOldInstallation() {
    echo -e "${GREEN}Searching and removing old $COIN_NAME files and configurations${NC}"
    systemctl stop $COIN_NAME.service > /dev/null 2>&1
    sudo killall $COIN_DAEMON > /dev/null 2>&1
	OLDKEY=$(awk -F'=' '/masternodeprivkey/ {print $2}' $CONFIGFOLDER/$CONFIG_FILE 2> /dev/null)
	if [ "$?" -eq "0" ]; then
    		echo -e "${CYAN}Saving Old Installation Genkey${NC}"
		echo -e $OLDKEY
	fi
    sudo ufw delete allow $COIN_PORT/tcp > /dev/null 2>&1
    rm rm -- "$0" > /dev/null 2>&1
    sudo rm -rf $CONFIGFOLDER > /dev/null 2>&1
    sudo rm -rf /usr/local/bin/$COIN_CLI /usr/local/bin/$COIN_DAEMON> /dev/null 2>&1
    sudo rm -rf /usr/bin/$COIN_CLI /usr/bin/$COIN_DAEMON > /dev/null 2>&1
    sudo rm -rf /tmp/*
    echo -e "${GREEN}* Done${NONE}";
}

checks() {
if [ -n "$(pidof $COIN_DAEMON)" ] || [ -e "$COIN_DAEMOM" ] ; then
  echo -e "${RED}$COIN_NAME is already installed.${NC}"
  exit 1
fi
}

prepare_system() {
echo -e "Preparing the VPS to setup. ${CYAN}$COIN_NAME${NC} ${RED}Masternode${NC}"
apt-get update >/dev/null 2>&1
DEBIAN_FRONTEND=noninteractive apt-get update > /dev/null 2>&1
DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y -qq upgrade >/dev/null 2>&1
apt-get install -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" zip unzip >/dev/null 2>&1
if [ "$?" -gt "0" ];
  then
    echo -e "${RED}Not all required packages were installed properly. Try to install them manually by running the following commands:${NC}\n"
    echo "apt-get update"
    echo "apt install -y zip unzip"
 exit 1
fi
clear
}

download_node() {
  echo -e "${GREEN}Downloading and Installing VPS $COIN_NAME Daemon${NC}"
  cd $TMP_FOLDER >/dev/null 2>&1
  wget -q $COIN_TGZ
  unzip $COIN_ZIP >/dev/null 2>&1
  chmod +x Linux/bin/$COIN_DAEMON Linux/bin/$COIN_CLI
  cp Linux/bin/$COIN_DAEMON Linux/bin/$COIN_CLI $COIN_PATH
  cd ~ >/dev/null 2>&1
  rm -rf $TMP_FOLDER >/dev/null 2>&1
  clear
}

create_config() {
  mkdir $CONFIGFOLDER >/dev/null 2>&1
  RPCUSER=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w10 | head -n1)
  RPCPASSWORD=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w22 | head -n1)
  echo rpcuser=$RPCUSER > $CONFIGFOLDER/$CONFIG_FILE
  echo rpcpassword=$RPCPASSWORD >> $CONFIGFOLDER/$CONFIG_FILE
  echo rpcport=$RPC_PORT >> $CONFIGFOLDER/$CONFIG_FILE
  echo rpcallowip=127.0.0.1 >> $CONFIGFOLDER/$CONFIG_FILE
  echo listen=1 >> $CONFIGFOLDER/$CONFIG_FILE
  echo server=1 >> $CONFIGFOLDER/$CONFIG_FILE
  echo daemon=0 >> $CONFIGFOLDER/$CONFIG_FILE
  echo port=$COIN_PORT >> $CONFIGFOLDER/$CONFIG_FILE
  echo  "" >> $CONFIGFOLDER/$CONFIG_FILE 
  echo logintimestamps=1 >> $CONFIGFOLDER/$CONFIG_FILE
  echo maxconnections=256 >> $CONFIGFOLDER/$CONFIG_FILE
  echo  "" >> $CONFIGFOLDER/$CONFIG_FILE
  echo masternode=1 >> $CONFIGFOLDER/$CONFIG_FILE
  echo masternodeaddr=NODE_IP:$COIN_PORT >> $CONFIGFOLDER/$CONFIG_FILE
  echo masternodeprivkey=MN_GENKEY >> $CONFIGFOLDER/$CONFIG_FILE
  echo  "" >> $CONFIGFOLDER/$CONFIG_FILE
  echo addnode=104.248.159.84 >> $CONFIGFOLDER/$CONFIG_FILE
  echo addnode=104.248.116.47 >> $CONFIGFOLDER/$CONFIG_FILE
  echo addnode=142.93.32.240 >> $CONFIGFOLDER/$CONFIG_FILE
  echo addnode=178.128.226.95 >> $CONFIGFOLDER/$CONFIG_FILE
  echo addnode=142.93.162.83 >> $CONFIGFOLDER/$CONFIG_FILE
}

##### Main #####
clear
purgeOldInstallation
checks
prepare_system
download_node
create_config