#!/bin/bash
# Update config file
CONFIG_FILE=/root/.kabberry/kabberry.conf
NODE_IP=$(curl -4 icanhazip.com)
if [ -z $MASTERNODE_GENKEY ]; then
    echo "MASTERNODE_GENKEY environment variable has to bet set!"
    exit 1
fi
sed -i 's/MN_GENKEY/'"$MASTERNODE_GENKEY"'/g' $CONFIG_FILE
sed -i 's/NODE_IP/'"$NODE_IP"'/g' $CONFIG_FILE

# Configure dupmn script
IFS=',' read -r -a KEYS <<< "$MASTERNODE_KEYS"
curl -sL https://raw.githubusercontent.com/KABBERRY/dupmn/master/dupmn_install.sh | sudo -E bash -

if [[ ! -d /root/.kabberry1/ ]]; then
wget -q https://raw.githubusercontent.com/KABBERRY/dupmn/master/profiles/Kabberry.dmn
dupmn profadd Kabberry.dmn Kabberry || true
for i in ${!KEYS[@]}
do
    dupmn install Kabberry -p "${KEYS[$i]}"
done
fi

for i in ${!KEYS[@]}
do
    kabberryd-$(($i+1)) &
done

# Start daemon
/usr/bin/kabberryd